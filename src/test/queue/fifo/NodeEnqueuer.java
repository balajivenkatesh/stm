package test.queue.fifo;

import java.util.concurrent.Callable;

public class NodeEnqueuer implements Runnable {
	private FIFOQueueTest queueTest;
	private Integer start;
	private Integer number;
	private int threadNo;

	public NodeEnqueuer(FIFOQueueTest queueTest, Integer temp, Integer number, int i) {
		this.queueTest = queueTest;
		this.start = temp;
		this.number = number;
		this.threadNo = i;
	}

	@Override
	public void run() {
		Integer n = start;
		while (n < start + number) {
			try {
				queueTest.tThreads[threadNo].doIt(new EnqInt(n));
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
			// try {
			// Thread.sleep(ThreadLocalRandom.current().nextInt(0, 100 + 1));
			// } catch (InterruptedException e) {
			// }
			n++;
		}
	}

	public class EnqInt implements Callable<Boolean> {
		private int n;

		public EnqInt(int temp) {
			this.n = temp;
		}

		@Override
		public Boolean call() throws Exception {
			queueTest.q.enqueue(n);
			queueTest.q.enqueue(n);
			return true;
		}
	}
}
