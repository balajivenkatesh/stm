package test.queue.integerSet;

import java.util.concurrent.Callable;

import transaction.AbortedException;
import transaction.PanicException;

public class SetNodeInserter implements Runnable {
	private final IntegerSetTest integerSetTest;
	private Integer start;
	private Integer number;
	private int threadNo;

	public SetNodeInserter(IntegerSetTest integerSetTest, Integer temp, Integer number, int i) {
		this.integerSetTest = integerSetTest;
		this.start = temp;
		this.number = number;
		this.threadNo = i;
	}

	@Override
	public void run() {
		Integer n = start;
		while (n < start + number) {
			try {
				integerSetTest.tThreads[threadNo].doIt(new EnqInt(n));
			} catch (AbortedException e) {
				continue;
			} catch (PanicException e) {
				break;
			} catch (Exception e) {
				continue;
			}
			n++;
		}
	}

	public class EnqInt implements Callable<Boolean> {
		private int n;

		public EnqInt(int temp) {
			this.n = temp;
		}

		@Override
		public Boolean call() throws Exception {
			SetNodeInserter.this.integerSetTest.q.insert(n);
			SetNodeInserter.this.integerSetTest.q.insert(n);
			return true;
		}
	}
}