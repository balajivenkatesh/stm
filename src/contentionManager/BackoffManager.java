package contentionManager;

import static contentionManager.Defaults.debug;

import java.util.Random;

import lockObject.LockObject;
import transaction.AbortedException;
import transaction.PanicException;
import transaction.Transaction;

public class BackoffManager extends ContentionManager {
	private static final int MIN_DELAY = 100;
	private static final int MAX_DELAY = 400;
	private Random random = new Random();
	private Transaction previous = null;
	private int delay = MIN_DELAY;

	@Override
	public void resolve(Transaction me, Transaction other) {
		if (other != previous) {
			previous = other;
			delay = MIN_DELAY;
		}
		if (delay < MAX_DELAY) {
			try {
				debug("Backing off... for " + delay);
				Thread.sleep(0, random.nextInt(delay));
			} catch (InterruptedException e) {
			}
			delay = 2 * delay;
		} else {
			debug("BTFOB...");
			other.abort();
			delay = MIN_DELAY;
		}
	}

	@Override
	public ResolveOpenReadWriteReturnValue resolveOpenRead(LockObject<?> o, Transaction me, Transaction other)
			throws AbortedException, PanicException {
		resolve(me, other);
		return new ResolveOpenReadWriteReturnValue();
	}

	@Override
	public ResolveOpenReadWriteReturnValue resolveOpenWrite(LockObject<?> o, Transaction me, Transaction other) throws AbortedException, PanicException {
		resolve(me, other);
		return new ResolveOpenReadWriteReturnValue();
	}

}
