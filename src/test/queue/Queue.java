package test.queue;

import transaction.AbortedException;
import transaction.PanicException;

public abstract class Queue<T> {
	public QueueNode<T> head;
	public QueueNode<T> tail;

	public Queue(T min, T max) throws AbortedException, PanicException {
		head = new TQueueNode<>(min);
		tail = new TQueueNode<>(max);
		head.setNext(tail);
		tail.setPrevious(head);
	}

	public void printNode(QueueNode<?> t) throws AbortedException, PanicException {
		System.out.print(t.getValue());
	}

	public QueueNode<?> getNextNode(QueueNode<?> t) throws AbortedException, PanicException {
		return t.getNext();
	}

	public QueueNode<?> getHead() {
		return head;
	}

	public QueueNode<T> getTail() {
		return tail;
	}

}