package contentionManager;

import lockObject.LockObject;
import transaction.AbortedException;
import transaction.PanicException;
import transaction.Transaction;

public abstract class ContentionManager {
	private static ThreadLocal<ContentionManager> local = new ThreadLocal<ContentionManager>() {
		protected ContentionManager initialValue() {
			while (true) {
				try {
					return (ContentionManager) Defaults.MANAGER.newInstance();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	};

	public static ContentionManager getLocal() {
		return local.get();
	}

	public static void setLocal(ContentionManager m) {
		local.set(m);
	}

	public abstract void resolve(Transaction me, Transaction other) throws AbortedException;

	public abstract ResolveOpenReadWriteReturnValue resolveOpenRead(LockObject<?> o, Transaction me, Transaction other)
			throws AbortedException, PanicException;

	public abstract ResolveOpenReadWriteReturnValue resolveOpenWrite(LockObject<?> o, Transaction me, Transaction other)
			throws AbortedException, PanicException;
}