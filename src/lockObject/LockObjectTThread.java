package lockObject;

import java.util.concurrent.Callable;

import transaction.TThread;

public class LockObjectTThread extends TThread {
	public LockObjectTThread() {
		setOnAbort(new LockObjectOnAbort());
		setOnCommit(new LockObjectOnCommit());
		setOnValidate(new LockObjectOnValidate());
	}
	
	public <T> T doIt(Callable<T> xaction) throws Exception {
		return super.doIt(xaction);
	}
}
