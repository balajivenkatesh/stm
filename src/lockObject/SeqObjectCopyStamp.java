package lockObject;

public class SeqObjectCopyStamp {
	private Object obj;
	private Long myStamp;
	
	public SeqObjectCopyStamp(Object o, Long myStamp) {
		this.obj = o;
		this.myStamp = myStamp;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}

	public Long getMyStamp() {
		return myStamp;
	}

	public void setMyStamp(Long myStamp) {
		this.myStamp = myStamp;
	}
	
}
