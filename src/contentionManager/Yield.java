package contentionManager;

import static contentionManager.Defaults.debug;

import lockObject.LockObject;
import transaction.AbortedException;
import transaction.PanicException;
import transaction.Transaction;

public class Yield extends ContentionManager {

	@Override
	public void resolve(Transaction me, Transaction other) throws AbortedException {
		if (other.getStatus() != Transaction.Status.ABORTED) {
			debug("Yielding...");
			throw new AbortedException();
		}
	}

	@Override
	public ResolveOpenReadWriteReturnValue resolveOpenRead(LockObject<?> o, Transaction me, Transaction other) throws AbortedException, PanicException {
		resolve(me, other);
		return new ResolveOpenReadWriteReturnValue();
	}

	@Override
	public ResolveOpenReadWriteReturnValue resolveOpenWrite(LockObject<?> o, Transaction me, Transaction other) throws AbortedException, PanicException {
		resolve(me, other);
		return new ResolveOpenReadWriteReturnValue();
	}
}
