package contentionManager;

import java.util.Random;

import lockObject.LockObject;
import transaction.AbortedException;
import transaction.PanicException;
import transaction.Transaction;
import transaction.Transaction.Status;

public class DependencyManager extends ContentionManager {
	private static final int MIN_DELAY = 1000;
	private static final int MAX_DELAY = 4000;
	private Random random = new Random();
	private Transaction previous = null;
	private int delay = MIN_DELAY;

	@Override
	public void resolve(Transaction me, Transaction other) {
		if (other != previous) {
			previous = other;
			delay = MIN_DELAY;
		}
		if (delay < MAX_DELAY) {
			try {
//				debug("Backing off... for " + delay);
				Thread.sleep(0, random.nextInt(delay));
			} catch (InterruptedException e) {
			}
			delay = 2 * delay;
		} else {
//			debug("BTFOB...");
			other.abort();
			delay = MIN_DELAY;
		}
	}

	@Override
	public ResolveOpenReadWriteReturnValue resolveOpenRead(LockObject<?> o, Transaction me, Transaction other)
			throws AbortedException, PanicException {
		return resolveReadWrite(o, me, other);
	}

	@Override
	public ResolveOpenReadWriteReturnValue resolveOpenWrite(LockObject<?> o, Transaction me, Transaction other)
			throws AbortedException, PanicException {
		return resolveReadWrite(o, me, other);
	}

	private ResolveOpenReadWriteReturnValue resolveReadWrite(LockObject<?> o, Transaction me, Transaction other)
			throws PanicException {
		Object othersObj = null;
		Long othersWriteStamp = Long.MAX_VALUE;
		if (other.getStatus() == Status.ABORTED) {
			othersObj = null;
		} else {
			othersObj = other.getObjectFromWriteSet(o);
			me.addDependency(o, other);
			if (other.getStatus() == Status.COMMITTED) {
				othersWriteStamp = other.getMyWriteStamp();
			}
		}
		return new ResolveOpenReadWriteReturnValue(false, othersObj, othersWriteStamp);
	}
}