package atomicObject;

import transaction.AbortedException;
import transaction.PanicException;

public abstract class AtomicObject<T extends Copyable<T>> {
	protected Class<T> internalClass;
	protected T internalInit;

	@SuppressWarnings("unchecked")
	public AtomicObject(T init) {
		internalInit = init;
		internalClass = (Class<T>) init.getClass();
	}

	public abstract T openRead() throws AbortedException, PanicException;

	public abstract T openWrite() throws AbortedException, PanicException;

	public abstract boolean validate();
	 
    @Override
    public String toString() {
    	return internalInit.toString();
    }
}