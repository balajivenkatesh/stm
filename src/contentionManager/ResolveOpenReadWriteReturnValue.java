package contentionManager;

public class ResolveOpenReadWriteReturnValue {
	private boolean checkLock;
	private Object returnValue;
	private Long stamp;
	
	public ResolveOpenReadWriteReturnValue(boolean checkLock, Object returnValue, Long aStamp) {
		this.checkLock = checkLock;
		this.returnValue = returnValue;
		this.stamp = aStamp;
	}

	public ResolveOpenReadWriteReturnValue() {
		this.checkLock = true;
		this.returnValue = null;
		this.stamp = Long.MAX_VALUE;
	}

	public Object getReturnValue() {
		return returnValue;
	}

	public Long getStamp() {
		return stamp;
	}

	public boolean isCheckLock() {
		return checkLock;
	}

	public void setCheckLock(boolean checkLock) {
		this.checkLock = checkLock;
	}

	public void setReturnValue(Object returnValue) {
		this.returnValue = returnValue;
	}

	public void setStamp(Long stamp) {
		this.stamp = stamp;
	}
}