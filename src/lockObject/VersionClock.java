package lockObject;

import java.util.concurrent.atomic.AtomicLong;

public class VersionClock {
	// global clock read and advanced by all
	private static AtomicLong global = new AtomicLong();
	// thread-local cached copy of global clock
	private static ThreadLocal<Long> local = new ThreadLocal<Long>() {
		protected Long initialValue() {
			return 0L;
		}
	};

	public static void setReadStamp() {
		local.set(global.get());
	}

	public static long getReadStamp() {
		return local.get();
	}

	public static void setWriteStamp() {
		local.set(global.incrementAndGet());
	}

	public static long getWriteStamp() {
		return local.get();
	}
}
