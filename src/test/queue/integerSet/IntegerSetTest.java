package test.queue.integerSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import lockObject.LockObjectTThread;
import test.queue.Queue;
import test.queue.QueueNode;
import transaction.TThread;

public class IntegerSetTest {
	private static final int MIN = Integer.MIN_VALUE;
	private static final int MAX = Integer.MAX_VALUE;

	public IntegerSet q;

	private int numThreads = 4;
	public TThread[] tThreads;

	private int number = 10;

	@org.junit.Before
	public void setUp() throws Exception {
		q = new IntegerSet(MIN, MAX);
		tThreads = new TThread[numThreads];
		for (int i = 0; i < numThreads; i++) {
			tThreads[i] = new LockObjectTThread();
		}
	}

	@org.junit.After
	public void tearDown() throws Exception {

	}

	public void printQ(Queue<?> t) throws Exception {
		QueueNode<?> node = t.getHead();
		System.out.println();
		while (node != null) {
			System.out.print(node.getValue() + " -> ");
			node = t.getNextNode(node);
		}
		System.out.println();
	}

	@SuppressWarnings("unchecked")
	public void printQVerifyInsert() throws Exception {
		HashMap<Integer, Integer> inQ = new HashMap<>();
		QueueNode<Integer> node = (QueueNode<Integer>) q.getHead();
		System.out.println();
		while (node != null) {
			System.out.print(node.getValue() + " -> ");
			if (node.getValue() != null && node.getValue() != MIN && node.getValue() != MAX) {
				int t = node.getValue();
				if (inQ.containsKey(t)) {
					inQ.put(t, inQ.get(t) + 1);
				} else {
					inQ.put(t, 1);
				}
			}
			node = (QueueNode<Integer>) q.getNextNode(node);
		}
		System.out.println();
		int count = 0;
		HashMap<Integer, Integer> exp = new HashMap<>();
		while (count != numThreads * number) {
			exp.put(count, 1);
			count++;
		}
		if (!exp.equals(inQ)) {
			System.out.println("Incorrect");
		} else {
			System.out.println("Correct");
		}
	}

	// @org.junit.Test
	public void testEnqueue() throws Exception {
		ExecutorService executor = Executors.newFixedThreadPool(numThreads);
		for (int i = 0; i < numThreads; i++) {
			Runnable task = new SetNodeInserter(this, i * number, number, i);
			executor.execute(task);
		}
		executor.shutdown();
		while (!executor.isTerminated()) {

		}
		executor.awaitTermination(10000, TimeUnit.MILLISECONDS);
		printQVerifyInsert();
	}

	public static long msLength = 1000;
	public static long endTime;
	private int numRepeat = 4;

	@org.junit.Test
	public void testOps() throws Exception {
		for (numThreads = 1; numThreads <= 8; numThreads++) {
			tThreads = new TThread[numThreads];
			for (int i = 0; i < numThreads; i++) {
				tThreads[i] = new LockObjectTThread();
			}
			int opsSum = 0;
			for (int i = 0; i < numRepeat; i++) {
				opsSum += runSetOps();
			}
			opsSum /= numRepeat;
			// System.out.println("\nAvg ops = " + opsSum);
			System.out.println((double) opsSum / msLength);
			// System.out.println();
		}
	}

	private int runSetOps() throws InterruptedException, BrokenBarrierException {
		final CyclicBarrier gate = new CyclicBarrier(numThreads + 1);
		ExecutorService executor = Executors.newFixedThreadPool(numThreads);
		ArrayList<SetOps> listTasks = new ArrayList<>();
		for (int i = 0; i < numThreads; i++) {
			SetOps task = new SetOps(this, gate, i);
			listTasks.add(task);
			executor.execute(task);
		}
		endTime = System.currentTimeMillis() + msLength;
		gate.await();
		executor.shutdown();
		executor.awaitTermination(30000, TimeUnit.MILLISECONDS);
		int sum = 0;
		for (SetOps task : listTasks) {
			int i = task.numOps;
			// System.out.print(task.threadNo + " = " + i + ", ");
			// System.out.print(i + ",");
			sum += i;
		}
		// System.out.println("\nTotal = " + sum);
		// System.out.println(sum);
		return sum;
	}

}