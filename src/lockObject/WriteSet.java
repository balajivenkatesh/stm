package lockObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import contentionManager.ContentionManager;
import transaction.AbortedException;
import transaction.Transaction;

public class WriteSet implements Iterable<Entry<LockObject<?>, Object>> {

	private static ThreadLocal<WriteSet> local = new ThreadLocal<>();
	private static ThreadLocal<Map<LockObject<?>, Object>> map = new ThreadLocal<Map<LockObject<?>, Object>>() {
		protected synchronized Map<LockObject<?>, Object> initialValue() {
			return null;
		}
	};
	
	public WriteSet(HashMap<LockObject<?>, Object> aMap) {
		map.set(aMap);
	}

	public static void clear() {
//		map.set(new HashMap<LockObject<?>, Object>());
	}

	public static Object get(LockObject<?> x) {
		return map.get().get(x);
	}

	public static WriteSet getLocal() {
		return local.get();
	}

	public static void setLocal(WriteSet ws) {
		local.set(ws);
	}

	public static void put(LockObject<?> x, Object y) {
		map.get().put(x, y);
	}

	public static boolean tryLock(long timeout, TimeUnit timeUnit) throws AbortedException {
		Transaction me = (Transaction) Transaction.getLocal();
		for (LockObject<?> x : map.get().keySet()) {
			try {
				while (!x.tryLock(timeout, timeUnit)) {
					ContentionManager.getLocal().resolve(me, x.owner);
					Thread.yield();
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
				throw new AbortedException();
			}
		}
		return true;
	}

	public static void unlock() {
		for (LockObject<?> x : map.get().keySet()) {
			x.unlock();
		}
	}

	@Override
	public Iterator<Entry<LockObject<?>, Object>> iterator() {
		return map.get().entrySet().iterator();
	}

	@Override
	public String toString() {
		return map.get().toString();
	}

}