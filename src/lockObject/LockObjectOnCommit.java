package lockObject;

import java.util.Map;

import atomicObject.Copyable;
import transaction.AbortedException;
import transaction.PanicException;
import transaction.Transaction;

public class LockObjectOnCommit implements Runnable {
	@SuppressWarnings("unchecked")
	public void run() {
		try {
			WriteSet writeSet = WriteSet.getLocal();
			VersionClock.setWriteStamp();
			long writeVersion = VersionClock.getWriteStamp();
			Transaction.getLocal().setMyWriteStamp(writeVersion);
			// System.out.println("OnCommit writeSet = " + writeSet);
			for (Map.Entry<LockObject<?>, Object> entry : writeSet) {
				LockObject<?> key = (LockObject<?>) entry.getKey();
				Copyable<?> destin = null;
				try {
					destin = (Copyable<?>) key.openRead();
				} catch (AbortedException | PanicException e) {
					System.out.println("Abort during OnCommit");
					throw new PanicException(e);
				}
				Copyable<Copyable<?>> source = (Copyable<Copyable<?>>) entry.getValue();
				source.copyTo(destin, destin);
				key.stamp = writeVersion;
			}

			WriteSet.unlock();
			WriteSet.clear();
			ReadSet.clear();
		} catch (Exception e) {
			System.out.println("OnCommit exception");
			e.printStackTrace();
		}
	}
}
