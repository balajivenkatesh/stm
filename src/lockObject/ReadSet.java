package lockObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ReadSet implements Iterable<LockObject<?>> {
	private static ThreadLocal<ReadSet> local = new ThreadLocal<>();
	private static ThreadLocal<Map<LockObject<?>, SeqObjectCopyStamp>> setLockObjectStamp = new ThreadLocal<Map<LockObject<?>, SeqObjectCopyStamp>>() {
		protected synchronized Map<LockObject<?>, SeqObjectCopyStamp> initialValue() {
			return new HashMap<>();
		}
	};

	public static void addWithStamp(LockObject<?> lockObject, Object anObject, Long aStamp) {
		if (!setLockObjectStamp.get().containsKey(lockObject)) {
			setLockObjectStamp.get().put(lockObject, new SeqObjectCopyStamp(anObject, aStamp));
		}
	}

	public static void clear() {
		setLockObjectStamp.get().clear();
	}

	public static ReadSet getLocal() {
		return local.get();
	}

	public static Object getLocalCopy(LockObject<?> x) {
		SeqObjectCopyStamp l = setLockObjectStamp.get().get(x);
		if (l == null) {
			return null;
		}
		return l.getObj();
	}

	public static Long getStamp(LockObject<?> x) {
		return setLockObjectStamp.get().get(x).getMyStamp();
	}
	
	public static void setLocal(ReadSet rs) {
		local.set(rs);
	}

	public static void updateStamp(LockObject<?> x, Long aStamp) {
		setLockObjectStamp.get().get(x).setMyStamp(aStamp);
	}

	@Override
	public Iterator<LockObject<?>> iterator() {
		return setLockObjectStamp.get().keySet().iterator();
	}

	@Override
	public String toString() {
		return setLockObjectStamp.get().toString();
	}
}
