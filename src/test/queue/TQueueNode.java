package test.queue;

import atomicObject.AtomicObject;
import lockObject.LockObject;
import transaction.AbortedException;
import transaction.PanicException;

public class TQueueNode<T> implements QueueNode<T> {
	AtomicObject<SQueueNode<T>> atomic;

	public TQueueNode(T value) {
		atomic = new LockObject<SQueueNode<T>>(new SQueueNode<>(value));
	}

	@Override
	public QueueNode<T> getNext() throws AbortedException, PanicException {
		QueueNode<T> next = atomic.openRead().getNext();
		if (!atomic.validate()) {
			throw new AbortedException();
		}
		return next;
	}

	@Override
	public QueueNode<T> getPrevious() throws PanicException, AbortedException {
		QueueNode<T> prev = atomic.openRead().getPrevious();
		if (!atomic.validate()) {
			throw new AbortedException();
		}
		return prev;
	}

	@Override
	public T getValue() throws AbortedException, PanicException {
		T tempVal = atomic.openRead().getValue();
		if (!atomic.validate()) {
			throw new AbortedException();
		}
		return tempVal;
	}

	@Override
	public void setNext(QueueNode<T> next) throws AbortedException, PanicException {
		atomic.openWrite().setNext(next);
	}

	@Override
	public void setPrevious(QueueNode<T> prev) throws PanicException, AbortedException {
		atomic.openWrite().setPrevious(prev);
	}

	@Override
	public void setValue(T value) throws AbortedException, PanicException {
		atomic.openWrite().setValue(value);
	}

	@Override
	public String toString() {
		return atomic.toString();
	}

}
