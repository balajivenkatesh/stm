package lockObject;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import atomicObject.AtomicObject;
import atomicObject.Copyable;
import contentionManager.ContentionManager;
import contentionManager.ResolveOpenReadWriteReturnValue;
import transaction.AbortedException;
import transaction.PanicException;
import transaction.Transaction;

public class LockObject<T extends Copyable<T>> extends AtomicObject<T> {
	public static final long TIMEOUT = 100;
	ReentrantLock lock;
	volatile long stamp;
	T version;
	volatile Transaction owner;

	public LockObject(T init) {
		super(init);
		version = init;
		lock = new ReentrantLock();
		stamp = 0;
	}

	@Override
	public T openRead() throws AbortedException, PanicException {
		Transaction me = Transaction.getLocal();
		switch (me.getStatus()) {
		case COMMITTED:
			return version;
		case ACTIVE:
			@SuppressWarnings("unchecked")
			T scratch = (T) WriteSet.get(this);
			if (scratch == null) {
				@SuppressWarnings("unchecked")
				T scratchReadSet = (T) ReadSet.getLocalCopy(this);
				if (scratchReadSet != null) {
					return scratchReadSet;
				}
				ResolveOpenReadWriteReturnValue resolveOpenReadReturnValue = null;
				while (lock.isLocked()) {
					resolveOpenReadReturnValue = ContentionManager.getLocal().resolveOpenRead(this, me, owner);
					if (!resolveOpenReadReturnValue.isCheckLock()) {
						break;
					}
					Thread.yield();
				}
				T copiedForReadSet = processOpenReadWriteReturnValue(resolveOpenReadReturnValue);
				return copiedForReadSet;
			} else {
				return scratch;
			}
		case ABORTED:
			throw new AbortedException();
		default:
			throw new PanicException("unexpected transaction state");
		}
	}

	@Override
	public T openWrite() throws AbortedException, PanicException {
		Transaction me = (Transaction) Transaction.getLocal();
		switch (me.getStatus()) {
		case COMMITTED:
			return version;
		case ACTIVE:
			@SuppressWarnings("unchecked")
			T scratch = (T) WriteSet.get(this);
			if (scratch == null) {
				@SuppressWarnings("unchecked")
				T scratchReadSet = (T) ReadSet.getLocalCopy(this);
				if (scratchReadSet != null) {
					WriteSet.put(this, scratchReadSet);
					return scratchReadSet;
				}
				ResolveOpenReadWriteReturnValue resolveOpenWriteReturnValue = null;
				while (lock.isLocked()) {
					resolveOpenWriteReturnValue = ContentionManager.getLocal().resolveOpenWrite(this, me, owner);
					if (!resolveOpenWriteReturnValue.isCheckLock()) {
						break;
					}
					Thread.yield();
				}
				T copiedForWriteSet = processOpenReadWriteReturnValue(resolveOpenWriteReturnValue);
				WriteSet.put(this, copiedForWriteSet);
				return copiedForWriteSet;
			}
			return scratch;
		case ABORTED:
			throw new AbortedException();
		default:
			throw new PanicException("unexpected transaction state");
		}
	}

	@SuppressWarnings("unchecked")
	private T processOpenReadWriteReturnValue(ResolveOpenReadWriteReturnValue resolveOpenReadWriteReturnValue)
			throws AbortedException {
		T copiedForReadSet = null;
		try {
			copiedForReadSet = internalClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
			throw new AbortedException();
		}
		T afterResolveOpenRead = null;
		if (resolveOpenReadWriteReturnValue != null) {
			afterResolveOpenRead = (T) resolveOpenReadWriteReturnValue.getReturnValue();
		}
		Long readSetStamp;
		if (afterResolveOpenRead != null) {
			afterResolveOpenRead.copyTo(copiedForReadSet, null);
			readSetStamp = resolveOpenReadWriteReturnValue.getStamp();
		} else {
			version.copyTo(copiedForReadSet, version);
			readSetStamp = this.stamp;
		}
		ReadSet.addWithStamp(this, copiedForReadSet, readSetStamp);
		return copiedForReadSet;
	}

	public boolean tryLock(long timeout, TimeUnit timeUnit) throws InterruptedException {
		if (lock.tryLock(timeout, timeUnit)) {
			owner = Transaction.getLocal();
			return true;
		}
		return false;
	}

	public void unlock() {
		if (lock.isLocked() && owner == Transaction.getLocal()) {
			lock.unlock();
		}
	}

	@Override
	public boolean validate() {
		Transaction.Status status = Transaction.getLocal().getStatus();
		switch (status) {
		case COMMITTED:
			return true;
		case ACTIVE:
			return stamp <= ReadSet.getStamp(this);
		case ABORTED:
			return false;
		}
		return false;
	}

}