package contentionManager;

public class Defaults {

	public static final Class<?> MANAGER = DependencyManager.class;
	private static final boolean printDebug = false;

	public static void debug(String s) {
		if (printDebug) {
			System.out.println(s);
		}
	}
}