package transaction;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicReference;

import lockObject.LockObject;
import lockObject.ReadSet;
import lockObject.VersionClock;
import lockObject.WriteSet;

public class Transaction {
	public static enum Status {
		ABORTED, ACTIVE, COMMITTED
	};

	private static ThreadLocal<Transaction> local = new ThreadLocal<Transaction>() {
		protected Transaction initialValue() {
			return new Transaction(Status.COMMITTED);
		}
	};

	public static Transaction getLocal() {
		return local.get();
	}

	public static void setLocal(Transaction transaction) {
		local.set(transaction);
	}

	private final AtomicReference<Status> status;

	private HashMap<LockObject<?>, Object> myWriteSet;

	private HashMap<LockObject<?>, Transaction> dependency;

	private final AtomicReference<Long> myWriteStamp;

	public Transaction() {
		status = new AtomicReference<Status>(Status.ACTIVE);
		VersionClock.setReadStamp();

		ReadSet.setLocal(new ReadSet());

		myWriteSet = new HashMap<>();
		WriteSet.setLocal(new WriteSet(myWriteSet));

		dependency = new HashMap<>();

		myWriteStamp = new AtomicReference<Long>(Long.MAX_VALUE);
	}

	private Transaction(Transaction.Status myStatus) {
		status = new AtomicReference<Status>(myStatus);
		myWriteStamp = new AtomicReference<Long>(Long.MAX_VALUE);
	}
	
	public boolean abort() {
		return status.compareAndSet(Status.ACTIVE, Status.ABORTED);
	}

	public void addDependency(LockObject<?> object, Transaction xaction) {
		dependency.put(object, xaction);
	}

	public boolean anyDependency() {
		return dependency.size() > 0;
	}

	public boolean commit() {
		return status.compareAndSet(Status.ACTIVE, Status.COMMITTED);
	}
	
	public HashMap<LockObject<?>, Transaction> getDependency() {
		return dependency;
	}

	public Long getMyWriteStamp() {
		return myWriteStamp.get();
	}

	public Object getObjectFromWriteSet(LockObject<?> x) {
		return myWriteSet.get(x);
	}

	public Status getStatus() {
		return status.get();
	}

	public void setMyWriteStamp(Long myWriteStamp) {
		this.myWriteStamp.set(myWriteStamp);
	}
}