package transaction;

import static contentionManager.Defaults.debug;

public class AbortedException extends Exception {

	private static final long serialVersionUID = 1L;

	public AbortedException() {
		debug("Aborting...");
	}
}
