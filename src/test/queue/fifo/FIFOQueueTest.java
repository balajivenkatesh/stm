package test.queue.fifo;

import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import lockObject.LockObjectTThread;
import test.queue.Queue;
import test.queue.QueueNode;
import transaction.TThread;

public class FIFOQueueTest {
	private static final int MIN = -1;
	private static final int MAX = -99;

	public FIFOQueue<Integer> q;
	public FIFOQueue<Integer> d;

	public TThread[] tThreads;
	private int numThreads = 5;

	private int number = 10;

	@org.junit.Before
	public void setUp() throws Exception {
		q = new FIFOQueue<>(MIN, MAX);
		d = new FIFOQueue<>(MIN, MAX);
		tThreads = new TThread[numThreads];
		for (int i = 0; i < numThreads; i++) {
			tThreads[i] = new LockObjectTThread();
		}
	}

	@org.junit.After
	public void tearDown() throws Exception {

	}

	public void printQ(Queue<?> t) throws Exception {
		QueueNode<?> node = t.getHead();
		System.out.println();
		while (node != null) {
			System.out.print(node.getValue() + " -> ");
			node = t.getNextNode(node);
		}
		System.out.println();
	}

	@SuppressWarnings("unchecked")
	public void printQVerifyDequeue(Queue<Integer> t) throws Exception {
		QueueNode<Integer> node = (QueueNode<Integer>) t.getHead();
		System.out.println();
		int p = -1;
		boolean last = true;
		boolean incorrect = false;
		while (node != null) {
			System.out.print(node.getValue() + " -> ");
			if (node.getValue() != null && node.getValue() != MIN && node.getValue() != MAX) {
				int i = node.getValue();
				if (!last) {
					if (i != p) {
						System.out.print(" (Incorrect) ");
						incorrect = true;
					}
				}
				last = !last;
				p = i;
			}
			node = (QueueNode<Integer>) t.getNextNode(node);
		}
		System.out.println();
		if (incorrect) {
			System.out.println("Incorrect");
		} else {
			System.out.println("Correct");
		}
	}

	@SuppressWarnings("unchecked")
	public void printQVerifyEnqueue() throws Exception {
		HashMap<Integer, Integer> inQ = new HashMap<>();
		QueueNode<Integer> node = (QueueNode<Integer>) q.getHead();
		System.out.println();
		while (node != null) {
			System.out.print(node.getValue() + " -> ");
			if (node.getValue() != null && node.getValue() != MIN && node.getValue() != MAX) {
				int t = node.getValue();
				if (inQ.containsKey(t)) {
					inQ.put(t, inQ.get(t) + 1);
				} else {
					inQ.put(t, 1);
				}
			}
			node = (QueueNode<Integer>) q.getNextNode(node);
		}
		System.out.println();
		int count = 0;
		HashMap<Integer, Integer> exp = new HashMap<>();
		while (count != numThreads * number) {
			exp.put(count, 2);
			count++;
		}
		if (!exp.equals(inQ)) {
			System.out.println("Incorrect");
		} else {
			System.out.println("Correct");
		}
	}

	@org.junit.Test
	public void testEnqueue() throws Exception {
		ExecutorService executor = Executors.newFixedThreadPool(numThreads);
		for (int i = 0; i < numThreads; i++) {
			Runnable task = new NodeEnqueuer(this, i * number, number, i);
			executor.execute(task);
		}
		executor.shutdown();
		while (!executor.isTerminated()) {

		}
		executor.awaitTermination(10000, TimeUnit.MILLISECONDS);
		printQVerifyEnqueue();
	}

	@org.junit.Test
	public void testDequeue() throws Exception {
		Runnable enqer = new NodeEnqueuer(this, 0, 40, 0);
		enqer.run();
		printQ(q);

		// Dequeuer sleeps 10 ms after every dequeue
		if (numThreads == 1) {
			numThreads = 2;
		}
		ExecutorService executor = Executors.newFixedThreadPool(numThreads - 1);
		for (int i = 1; i < numThreads; i++) {
			Runnable task = new NodeDequeuer(this, i);
			executor.execute(task);
		}
		executor.shutdown();
		executor.awaitTermination(10000, TimeUnit.MILLISECONDS);
		printQVerifyDequeue(d);
		printQ(q);
	}

}