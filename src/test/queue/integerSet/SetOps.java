package test.queue.integerSet;

import java.util.concurrent.Callable;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ThreadLocalRandom;

public class SetOps implements Runnable {
	private final IntegerSetTest integerSetTest;
	public int threadNo;
	public int numOps;
	private CyclicBarrier gate;

	public SetOps(IntegerSetTest integerSetTest, CyclicBarrier gate, int i) {
		this.integerSetTest = integerSetTest;
		this.threadNo = i;
		this.gate = gate;
		this.numOps = 0;
	}

	@Override
	public void run() {
		try {
			gate.await();
		} catch (Exception e) {
		}
		long eT = IntegerSetTest.endTime;
		while (System.currentTimeMillis() < eT) {
			try {
				this.integerSetTest.tThreads[threadNo].doIt(new Callable<Boolean>() {
					@Override
					public Boolean call() throws Exception {
						int i = ThreadLocalRandom.current().nextInt(0, 255 + 1);
//						int j = 255 - i;
						int rnd = ThreadLocalRandom.current().nextInt(0, 100 + 1);
						if (rnd < 1) {
							integerSetTest.q.insert(i);
							return integerSetTest.q.insert(i);
						} else if (rnd < 2) {
							integerSetTest.q.delete(i);
							return integerSetTest.q.delete(i);
						} else {
							integerSetTest.q.contains(i);
							return integerSetTest.q.contains(i);
						}
					}
				});
				numOps += 2;
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
		}
	}
}