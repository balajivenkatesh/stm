package transaction;

public class PanicException extends Exception {

	public PanicException(Exception e) {
		e.printStackTrace();
		System.exit(-1);
	}

	public PanicException(String string) {
		System.out.println(string);
		printStackTrace();
		System.exit(-1);
	}

	private static final long serialVersionUID = 1L;

}
