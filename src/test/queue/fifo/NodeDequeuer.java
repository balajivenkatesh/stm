package test.queue.fifo;

import java.util.concurrent.Callable;

public class NodeDequeuer implements Runnable {
	private FIFOQueueTest queueTest;
	private int threadNo;

	public NodeDequeuer(FIFOQueueTest queueTest, int i) {
		this.queueTest = queueTest;
		this.threadNo = i;
	}

	@Override
	public void run() {
		Integer i = null;
		do {
			try {
				i = queueTest.tThreads[threadNo].doIt(new DeqInt(threadNo));
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
		} while (i != null);
	}

	public class DeqInt implements Callable<Integer> {
		private int n;

		public DeqInt(int temp) {
			this.n = temp;
		}

		@Override
		public Integer call() throws Exception {
			{
				Integer e = queueTest.q.deque();
				if (e != null) {
					queueTest.d.enqueue(e + n * 1000);
				}
			}
			{
				Integer e = queueTest.q.deque();
				if (e != null) {
					queueTest.d.enqueue(e + n * 1000);
				}
				return e;
			}
		}
	}
}
