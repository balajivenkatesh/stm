package test.queue.fifo;

import test.queue.Queue;
import test.queue.QueueNode;
import test.queue.TQueueNode;
import transaction.AbortedException;
import transaction.PanicException;

public class FIFOQueue<T> extends Queue<T> {
	
	public FIFOQueue(T min, T max) throws AbortedException, PanicException {
		super(min, max);
	}

	public void enqueue(T val) throws AbortedException, PanicException {
		if (val == null) {
			throw new PanicException("Cannot enqueue null.");
		}
		QueueNode<T> oldTail = tail.getPrevious();
		TQueueNode<T> temp = new TQueueNode<T>(val);
		oldTail.setNext(temp);
		temp.setPrevious(oldTail);
		temp.setNext(tail);
		tail.setPrevious(temp);
	}
	
	public T deque() throws AbortedException, PanicException {
		if (head.getNext() == tail) {
			return null;
		}
		QueueNode<T> temp = head.getNext();
		head.setNext(temp.getNext());
		if (temp.getNext() == tail) {
			tail.setPrevious(head);
		}
		return temp.getValue();
	}

}