package atomicObject;

public interface Copyable<T> {
	void copyTo(T target, T destin);
}