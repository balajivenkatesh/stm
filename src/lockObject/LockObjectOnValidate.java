package lockObject;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import contentionManager.ContentionManager;
import transaction.AbortedException;
import transaction.PanicException;
import transaction.Transaction;
import transaction.Transaction.Status;

public class LockObjectOnValidate implements Callable<Boolean> {
	public Boolean call() throws Exception {
		try {
			int depCount = Transaction.getLocal().getDependency().size();
			while (depCount > 0) {
				Iterator<Entry<LockObject<?>, Transaction>> it = Transaction.getLocal().getDependency().entrySet()
						.iterator();
				while (it.hasNext()) {
					Entry<LockObject<?>, Transaction> e = it.next();
					LockObject<?> clashObj = e.getKey();
					Transaction other = e.getValue();
					if (other.getStatus() == Status.COMMITTED) {
						Long otherStamp = other.getMyWriteStamp();
						if (otherStamp != Long.MAX_VALUE) {
							ReadSet.updateStamp(clashObj, otherStamp);
							it.remove();
							depCount--;
//							if (depCount == 0) {
//								break;
//							}
						}
					} else if (other.getStatus() == Status.ABORTED) {
						return false;
					}
				}
//				if (Transaction.getLocal().getDependency().size() == 0) {
//					System.out.println("Dependencies = " + depCount);
//				}
				if (depCount == 0) {
					break;
				}
				Thread.yield();
			}
			
			// WriteSet writeSet = WriteSet.getLocal();
			// System.out.println("OnValidate writeSet = " + writeSet + ",
			// readSet" + readSet);
			if (!WriteSet.tryLock(LockObject.TIMEOUT, TimeUnit.MILLISECONDS)) {
				return false;
			}
			ReadSet readSet = ReadSet.getLocal();
			Transaction me = (Transaction) Transaction.getLocal();
			for (LockObject<?> x : readSet) {
				while (x.lock.isLocked() && !x.lock.isHeldByCurrentThread()) {
					try {
						ContentionManager.getLocal().resolve(me, x.owner);
						Thread.yield();
					} catch (AbortedException e) {
						return false;
					}
				}
				if (x.stamp > ReadSet.getStamp(x)) {
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			System.out.println("OnValidate exception");
			throw new PanicException(e);
		}
	}
}
