package transaction;

import static contentionManager.Defaults.debug;

import java.util.concurrent.Callable;

public abstract class TThread extends java.lang.Thread {
	private Runnable onAbort = null;
	private Runnable onCommit = null;
	private Callable<Boolean> onValidate = null;

	public void setOnAbort(Runnable onAbort) {
		this.onAbort = onAbort;
	}

	public void setOnCommit(Runnable onCommit) {
		this.onCommit = onCommit;
	}

	public void setOnValidate(Callable<Boolean> onValidate) {
		this.onValidate = onValidate;
	}

	public <T> T doIt(Callable<T> xaction) throws Exception {
		T result = null;
		while (true) {
			Transaction me = new Transaction();
			Transaction.setLocal(me);
			try {
				result = xaction.call();
			} catch (AbortedException e) {
				debug("TThread aborted...");

				me.abort();
				onAbort.run();
				continue;
			} catch (Exception e) {
				throw new PanicException(e);
			}
			if (onValidate.call()) {
				if (me.commit()) {
					onCommit.run();
					return result;
				}
			}
			debug("TThread validate failed...");

			me.abort();
			onAbort.run();
		}
	}
}