package test.queue;

import transaction.AbortedException;
import transaction.PanicException;

public interface QueueNode<T> {
    public QueueNode<T> getNext() throws PanicException, AbortedException;

    public void setNext(QueueNode<T> next) throws PanicException, AbortedException;

    public QueueNode<T> getPrevious() throws PanicException, AbortedException;

    public void setPrevious(QueueNode<T> prev) throws PanicException, AbortedException;
    
    public T getValue() throws PanicException, AbortedException;

    public void setValue(T value) throws PanicException, AbortedException;
}
