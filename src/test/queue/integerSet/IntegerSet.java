package test.queue.integerSet;

import test.queue.Queue;
import test.queue.QueueNode;
import test.queue.TQueueNode;
import transaction.AbortedException;
import transaction.PanicException;

public class IntegerSet extends Queue<Integer> {

	public IntegerSet(Integer min, Integer max) throws AbortedException, PanicException {
		super(min, max);
	}

	public boolean insert(Integer val) throws AbortedException, PanicException {
		if (val == null) {
			throw new PanicException("Cannot insert null.");
		}
		QueueNode<Integer> temp = head.getNext();
		while (temp.getNext() != null && temp.getValue() < val) {
			temp = temp.getNext();
		}
		if (temp.getValue() == val) {
			return false;
		}
		TQueueNode<Integer> newNode = new TQueueNode<Integer>(val);
		QueueNode<Integer> prev = temp.getPrevious();
		prev.setNext(newNode);
		newNode.setNext(temp);
		temp.setPrevious(newNode);
		newNode.setPrevious(prev);
		return true;
	}

	public boolean delete(Integer val) throws AbortedException, PanicException {
		QueueNode<Integer> temp = head.getNext();
		while (temp.getNext() != null && temp.getValue() < val) {
			temp = temp.getNext();
		}
		if (temp.getValue() != val) {
			return false;
		}
		QueueNode<Integer> prev = temp.getPrevious();
		QueueNode<Integer> next = temp.getNext();
		prev.setNext(next);
		next.setPrevious(prev);
		return true;
	}

	public boolean contains(Integer val) throws AbortedException, PanicException {
		if (val == null) {
			throw new PanicException("Cannot contain null.");
		}
		QueueNode<Integer> temp = head.getNext();
		while (temp.getNext() != null && temp.getValue() < val) {
			temp = temp.getNext();
		}
		if (temp.getValue() == val) {
			return true;
		}
		return false;
	}
}