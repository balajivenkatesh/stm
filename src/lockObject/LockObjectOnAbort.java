package lockObject;

public class LockObjectOnAbort implements Runnable {
	public void run() {
		WriteSet.unlock();
		WriteSet.clear();
		ReadSet.clear();
	}
}
