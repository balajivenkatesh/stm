package test.queue;

import java.util.concurrent.atomic.AtomicReference;

import atomicObject.Copyable;

public class SQueueNode<T> implements QueueNode<T>, Copyable<SQueueNode<T>> {
	private T value;
	private QueueNode<T> next;
	private QueueNode<T> prev;
	private AtomicReference<Integer> lock = new AtomicReference<Integer>(0);

	public SQueueNode() {
		value = null;
		next = null;
		prev = null;
	}

	public SQueueNode(T value) {
		this.value = value;
		this.next = null;
		this.prev = null;
	}

	public QueueNode<T> getNext() {
		return next;
	}

	public void setNext(QueueNode<T> next) {
		this.next = next;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	@Override
	public void copyTo(SQueueNode<T> target, SQueueNode<T> lockThis) {
		if (lockThis != null) {
			while (lockThis.lock.compareAndSet(0, 1)) {
				Thread.yield();
			}
		}
		target.setValue(this.value);
		target.setNext(this.next);
		target.setPrevious(this.prev);
		if (lockThis != null) {
			lockThis.lock.set(0);
		}
	}

	@Override
	public String toString() {
		String s = "{Value = ";
		if (value == null) {
			s += "Null";
		} else {
			s += value.toString();
		}
		s += "}";
		return s;
	}

	@Override
	public QueueNode<T> getPrevious() {
		return prev;
	}

	@Override
	public void setPrevious(QueueNode<T> prev) {
		this.prev = prev;
	}
}
